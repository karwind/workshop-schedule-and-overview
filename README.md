# Purpose of Workshop Series

To promote collaborative work here at DTU Wind Energy through training and 
education of researchers in scientific Python and related tools.

# 2017 Workshop Schedule

Please note that this schedule is subject to change. All workshops occur in
H. H. Koch.

1.	**Introduction to Anaconda**
    - 27. sep 13:00
    - Spyder, Jupyter, Anaconda prompt, creating and working in environments,
installing new packages (pip vs conda)
2.	**Getting started with Python**
    - 4. okt 10:00
    - Switching from Matlab, most common packages (numpy, scipy, pandas,
matplotlib), PEP8
3.	**Collaborating with Python**
    - 17. okt 10:00
    - Basic git, installing a package from GitHub/GitLab, making your own package
4.	**How to speed up your code**
    - CANCELLED
    - Cython and multiprocessing
5.	**Test-driven development and documentation**
    - 16. nov 13:00
    - Writing and running tests, continuous integration, sphinx
6.	**Python tools/resources here at DTU**
    - 21. nov 10:00
    - Wind Energy Toolbox, HawtOpt2, etc.

# Workshop Resources

- [Description of workshop series and schedule](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/workshop-schedule-and-overview)
- [Workshop #1](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/1-intro-to-conda)
- [Workshop #2](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/2-getting-started)
- [Workshop #3](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/3-collaborating-with-python)
- [Workshop #4](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/4-speeding-up-python)
- [Workshop #5](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/5-tests-and-documentation)
- [Workshop #6](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/6-risoe-python-tools)

# Contact

Jenni Rinker  
rink@dtu.dk